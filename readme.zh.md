# github2gitea

Language: [en](readme.md) | **中国人**

Github 仓库迁移至 Gitea 脚本

迁移 github 私有/公有仓库到 gitea 是一件容易的事情，不过由于网络原因，部分较大的项目可能会失败。此脚本会自动过滤已经迁移完成的仓库，因此重复执行脚本直至 gitea 中的仓库和 github 中的仓库数量一致，即表示迁移完成。gitea 迁移失败的仓库会被系统自动删除，因此可能出现仓库数量浮动。为了确保代码资产安全，您在迁移完成后应该间隔十分钟再次执行，直到 gitea 中的仓库和 github 中的仓库数量仍然一致。

需要注意，使用该脚本前，需创建一个Github Token，并且需要临时关闭Two-factor authentication。

<https://github.com/go-gitea/gitea/issues/479>
