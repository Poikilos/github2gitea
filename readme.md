# github2gitea

Language: **en** | [中国人](readme.zh.md)

Github repository migrated to Gitea script

Migrating github private/public repositories to gitea is an easy task,
but due to network reasons, some larger projects may fail. This script
will automatically filter the repositories that have been migrated, so
repeat the script until the number of repositories in gitea and the
number of repositories in github are the same, which means that the
migration is complete. Repositories that fail to migrate by gitea will
be automatically deleted by the system, so the number of repositories
may fluctuate. In order to ensure the safety of code assets, you should
execute it again at ten minutes after the migration is completed, until
the number of repositories in gitea and the number of repositories in
github are still the same.

It should be noted that before using this script, a Github Token needs
to be created, and Two-factor authentication needs to be temporarily
disabled.

## Migrating issues
Since this only uses the Gitea API's migrate feature, it will only get
issues with the "mirror" option turned off! For a discussion of the
issue, see:
- [Migrate issues and wikis options when migrating from github](https://github.com/go-gitea/gitea/issues/479)
  - Note that
    <https://git.jonasfranz.software/JonasFranzDEV/gitea-github-migrator>
    moved to <https://gitea.com/gitea/migrator/>. It says it accepts
    issues, but doesn't if "mirror" is checked!
- [Move issues from one repository to another on the same organization](https://github.com/go-gitea/gitea/issues/2991)
- [Moving issues across repositories](https://github.com/go-gitea/gitea/issues/453)

Note that the "Issues" option is grayed out in the migrate window if
the **mirror** option is checked!
