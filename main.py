#!/usr/bin/env python3
'''
This script simply uses the migrate feature in the Gitea API URL.
See more complete solutions in readme.md.
'''
import requests
from requests.adapters import HTTPAdapter
import time
import json
import platform
import os
syncRepos
profile = None
appdatas = None
if platform.system() == "Windows":
    profile = os.environ["USERPROFILE"]
    appdatas = os.path.join(profile, "AppData", "Local")
else:
    profile = os.environ["HOME"]
    appdatas = os.path.join(profile, ".config")

appdata = os.path.join(appdatas, "github2gitea")

if not os.path.isdir(appdata):
    os.makedirs(appdata)

metaPath = os.path.join(appdata, 'finishedRepo.json')

requiredNames = [
    'GITEA_URL',
    'GITEA_USERNAME',
    'GITEA_PASSWORD',
    'GITEA_ORGNAME_OR_USERNAME',
    'GITHUB_ORGNAME',
    'GITHUB_TOKEN',
    'GITHUB_USERNAME',
    'GITHUB_PASSWORD',
]

def syncRepos(options):
    GITEA_URL = 'https://domain'
    GITEA_USERNAME = 'username'
    GITEA_PASSWORD = '***'
    GITEA_ORGNAME_OR_USERNAME = 'orgName'
    GITHUB_ORGNAME = 'orgName'
    GITHUB_TOKEN = '***************************************'
    GITHUB_USERNAME = 'yi-ge'
    GITHUB_PASSWORD = '***'
    finishedRepo = json.load(open(metaPath, 'r'))

    # Get user Gitea
    response_user = requests.get(
        f'{GITEA_URL}/api/v1/users/{GITEA_ORGNAME_OR_USERNAME}')

    verify = False
    if response_user.status_code != 200:
        msg = 'Error user Gitea.'
        print(msg)
        return {"error": msg}
    uid = response_user.json()['id']
    for n in range(1000):
        page = n + 1
        n = page
        response_github = requests.get(
            f'https://api.github.com/orgs/{GITHUB_ORGNAME}/repos?' +
            f'per_page=100&page={page}',
            headers={'Authorization': 'token ' + GITHUB_TOKEN})

        # List Github repos
        if response_github.status_code != 200:
            print(response_github.status_code)
            print('Error list Github repos.')
            continue
            # return {"error":str(response_github.status_code)}
        if not len(response_github.json()):
            msg = "All clone finished!"
            if verify:
                print(msg)
            else:
                page = 1
                verify = True
            continue
            # return {"status": msg}

        for repo in response_github.json():
            repo_clone_url = repo['clone_url']
            repo_name = repo['name']
            description = repo['description']

            print('Creating repository: ' + repo_name)
            print(repo_clone_url)

            s = requests.Session()
            s.mount('http://', HTTPAdapter(max_retries=3))
            s.mount('https://', HTTPAdapter(max_retries=3))

            response_migrate = s.post(
                f'{GITEA_URL}/api/v1/repos/migrate',
                json={
                    'clone_addr': repo_clone_url.replace(
                        'https://github.com',
                        f'https://{GITHUB_USERNAME}:' +
                        GITEA_PASSWORD +
                        '@github.com'),
                    'mirror': False,
                    'private': True,
                    "issues": True,
                    "labels": True,
                    "milestones": True,
                    "pull_requests": True,
                    "releases": True,
                    "wiki": True,
                    'repo_name': repo_name,
                    'uid': uid,
                    'description': description
                },
                auth=(GITEA_USERNAME, GITEA_PASSWORD),
                timeout=120
            )
            if (response_migrate.status_code == 409
                    or response_migrate.json()['id'] > 0):
                finishedRepo.append({
                    'name': repo_clone_url,
                    'time': time.strftime(
                        '%Y-%m-%d %H:%M:%S',
                        time.localtime()
                    ),
                    'created':
                        response_migrate.status_code == 409
                })
                open(metaPath, 'w').write(
                    json.dumps(finishedRepo))
                if (response_migrate.status_code == 409):
                    print(
                        repo_name +
                        ': The repository already exists.'
                    )
                else:
                    print(
                        repo_name +
                        ': Clone repository created!'
                    )
                    # time.sleep(8)
            else:
                print(repo_name + ': Clone repository error!')
                print(response_migrate.json())


if __name__ == "__main__":
    syncRepos()
